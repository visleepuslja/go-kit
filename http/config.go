package http

const (
	DefaultShutdownDuration = 5
)

type Config struct {
	Addr             string `json:"addr"`
	ShutdownDuration int    `json:"shutdownDuration"`
	ApiRoot          string `json:"apiRoot"`
}

func WithAppliedDefaults(config Config) Config {
	if config.ShutdownDuration == 0 {
		config.ShutdownDuration = DefaultShutdownDuration
	}
	return config
}
