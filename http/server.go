package http

import (
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

type Server interface {
	Run(configurationFunc ConfigurationFunc) error
	Close() error
}

type ConfigurationFunc func(engine *gin.Engine)

func New(config Config) Server {
	return &server{
		config:  WithAppliedDefaults(config),
		closeCh: make(chan error, 1),
	}
}

type server struct {
	config  Config
	closeCh chan error
}

func (s *server) Run(configurationFunc ConfigurationFunc) error {
	var err error
	engine := gin.Default()
	configurationFunc(engine)
	server := &http.Server{
		Addr:    s.config.Addr,
		Handler: engine,
	}

	errCh := make(chan error, 1)
	go func() {
		errCh <- server.ListenAndServe()
	}()

	select {
	case err = <-errCh:
	case <-s.closeCh:
		defer func() {
			s.closeCh <- err
		}()
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(s.config.ShutdownDuration))
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			log.Printf("http: shutdown error: %v", err)
		}
		err = server.Close()
	}

	return err
}

func (s *server) Close() error {
	s.closeCh <- nil
	return <-s.closeCh
}
