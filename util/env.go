package util

import (
	"bitbucket.org/visleepuslja/go-kit/errors"
	"encoding/json"
	"io/ioutil"
	"os"
)

func ParseFileFromEnv(env string, model interface{}) error {
	path, ok := os.LookupEnv(env)
	if !ok {
		return errors.New("config file path is not set")
	}
	var data []byte
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, model)
}
