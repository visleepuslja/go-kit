package util

import (
	"os"
	"os/signal"
	"syscall"
)

func SignalChannel(signals ...os.Signal) chan os.Signal {
	sigCh := make(chan os.Signal, 1)
	if len(signals) == 0 {
		signal.Notify(sigCh, os.Interrupt, os.Kill, syscall.SIGTERM)
	} else {
		signal.Notify(sigCh, signals...)
	}
	return sigCh
}
