package errors

import (
	"errors"
	"fmt"
)

var (
	NotFound            = errors.New("not found")
	AlreadyExists       = errors.New("already exists")
	InternalServerError = errors.New("internal server error")
)

func New(text string) error {
	return errors.New(text)
}

func Format(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}
