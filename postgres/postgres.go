package postgres

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"log"
)

type Postgres struct {
	*pg.DB
}

func New(config Config, logger *log.Logger) (*Postgres, error) {
	conn := pg.Connect(&pg.Options{
		Addr:     config.Addr,
		User:     config.User,
		Password: config.Password,
		Database: config.Database,
	})

	if logger != nil {
		pl := newPlog(logger)
		conn.AddQueryHook(pl)
	}

	if _, err := conn.Exec("select 1"); err != nil {
		return nil, err
	}

	return &Postgres{DB: conn}, nil
}

func (p *Postgres) Close() error {
	return p.DB.Close()
}

func (p *Postgres) Database(ctx context.Context) *Database {
	return &Database{
		Query: func(ctx context.Context, models ...interface{}) *orm.Query {
			db := p.DB
			if ctx != nil {
				db = db.WithContext(ctx)
			}
			return db.Model(models...)
		},
	}
}

func (p *Postgres) Tx(ctx context.Context) (*Database, RollbackFunc, CommitFunc, error) {
	db := p.DB
	if ctx != nil {
		db = db.WithContext(ctx)
	}

	tx, err := db.Begin()
	if err != nil {
		return nil, nil, nil, err
	}

	return &Database{
			Query: func(ctx context.Context, models ...interface{}) *orm.Query {
				return tx.Model(models...)
			},
		}, func() error {
			return Format(tx.Rollback())
		}, func() error {
			return Format(tx.Commit())
		}, nil
}
