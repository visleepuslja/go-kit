package postgres

type Config struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Addr     string `json:"addr"`
	Database string `json:"database"`
}
