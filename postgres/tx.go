package postgres

type RollbackFunc = func() error
type CommitFunc = func() error
