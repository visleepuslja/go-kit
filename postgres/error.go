package postgres

import (
	"github.com/go-pg/pg"
	"log"
)

func Format(err error) error {
	if err == pg.ErrNoRows {
		return errors.NotFound
	}
	if pgErr, ok := err.(pg.Error); ok {
		code := pgErr.Field('C')
		if code == "23505" {
			return errors.AlreadyExists
		}
		if code == "23503" {
			return errors.InternalServerError
		}
		return errors.Format("[postgres] error: %s", pgErr.Field('M'))
	}
	if err != nil {
		log.Printf("[postgres] error: %+v", err)
		return errors.InternalServerError
	}
	return err
}
