package postgres

import (
	"github.com/go-pg/pg"
	"log"
)

type pLog struct {
	logger *log.Logger
}

func newPlog(logger *log.Logger) *pLog {
	return &pLog{
		logger: logger,
	}
}

func (l *pLog) BeforeQuery(query *pg.QueryEvent) {}
func (l *pLog) AfterQuery(query *pg.QueryEvent) {
	l.logger.Print(query.FormattedQuery())
}
