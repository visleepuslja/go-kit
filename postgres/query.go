package postgres

import (
	"context"
	"github.com/go-pg/pg/orm"
)

type Query func(ctx context.Context, models ...interface{}) *orm.Query
